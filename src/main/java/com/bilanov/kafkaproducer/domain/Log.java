package com.bilanov.kafkaproducer.domain;

public class Log {
    private int id;
    private String request;
    private String response;

    public Log(int id, String request, String response) {
        this.id = id;
        this.request = request;
        this.response = response;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
