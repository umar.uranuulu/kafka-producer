package com.bilanov.kafkaproducer.service;

import com.bilanov.kafkaproducer.domain.Log;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class LogService {

    private final KafkaTemplate<String, Log> KafkaJsonTemplate;
    String TOPIC_NAME = "aggregator";

    public LogService(KafkaTemplate<String, Log> KafkaJsonTemplate) {
        this.KafkaJsonTemplate = KafkaJsonTemplate;
    }

    public String send(Log log) {
        KafkaJsonTemplate.send(TOPIC_NAME, log);
        return "Message published successfully";
    }
}
