package com.bilanov.kafkaproducer.controller;

import com.bilanov.kafkaproducer.domain.Log;
import com.bilanov.kafkaproducer.service.LogService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/producer/log")
public class LogController {

    private final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @PostMapping(value = "/send",consumes = {"application/json"},produces = {"application/json"})
    public String postJsonMessage(@RequestBody Log log){
        return logService.send(log);
    }

}
